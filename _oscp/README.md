---
Title:  'Offensive Security Certified Professional'
Name:   'OSCP'
UUID:   'rwx.gg/oscp'

Description: |

  THIS DOCUMENT IS OLD AND HERE FOR LATER SPELUNKING

  The Offensive Security Certified Professional is a proprietary
  certificate offered by [Offensive
  Security](https://www.offensive-security.com). This informal list of
  requirements is meant to capture the skills, knowledge, and abilities
  required before attempting the test.

  The OSCP is primarily focused on breaking into systems, becoming
  a Pentester, aka Offensive Security Professional but can equally serve
  as a building block toward other more general security professions such
  as a Cybersecurity Analyst, or Engineer.

  This list may be incomplete, but serves as a guide.

Prerequisite:
  -P: 'rwx.gg/gltp'

Requirements:
  - R: Setup Workstation Suitable for Taking OffSec Proctored Exams
    D: |
      * 64-bit Dual Core CPU (2.2 GHz per core)
      * 8+GB RAM
      * Windows 8.1 x64 / OSX Yosemite / MacOS/Kali 2017.x/ Debian 9.3/Ubuntu 17.10 
      * Good Web Cam
      * Working, tested graphics with prop drivers for Nvidia (if Nvidia)
      * Java SE Runtime Environment 9
      * VMware Player (Latest version), VMware Workstation 8.0, VMware Fusion 7.0 (no VirtualBox)
      * Google Chrome 57.0
      * Minimum 5mbps Download/ 1mbps Upload speeds
      * Stable connection that does not drop

  - R: 'Explain why OSCP differs from other credentials.'
  - R: 'Understand exam restrictions.'
    - R: 'Use of alternatives to what is in course is okay.'
  - R: 'Execute a buffer overflow attack.'
  - R: 'Tunnel using Secure Shell'
  - R: 'Explain and demonstrate use of pentesting tools.'
    - R: 'Explain and demonstrate use of Metasploit'
    - R: 'Develop your own tools and exploits.'
    - R: 'Code proficiently in core security languages.'
      - R: 'Code in Bash.'
      - R: 'Code in Python (2 more than 3).'
      - R: 'Code in PowerShell.'
  - R: 'Create reports efficiently.'
  - R: 'Use practical pentesting tools.'
    - R: 'Use Nikto.'
    - R: 'Use Burp Free.'
    - R: 'Use Dir/Gobuster.'
    - R: 'Use Netcat.'
      - R: 'Connect to TCP/UDP port with Netcat.'
      - R: 'Listen to TCP/UDP port with Netcat.'
      - R: 'Transfer files with Netcat.'
      - R: 'Execute remote commands with Netcat.'
    - R: 'Use Socat.'
    - R: ''
      - R: 'Explain difference between Netcat and Socat.'
      - R: 'Transfer files with Socat.'
      - R: 'Implement a reserve shell with Socat.'
      - R: 'Implement a bind shell with Socat.'
    - R: 'Use PowerShell'
      - R: 'Transfer files with PowerShell.'
      - R: 'Implement a reserve shell with PowerShell.'
      - R: 'Implement a bind shell with PowerShell.'
    - R: 'Use PowerCat'
      - R: 'Transfer files with PowerCat.'
      - R: 'Implement a reserve shell with PowerCat.'
      - R: 'Implement a bind shell with PowerCat.'
    - R: 'Use Wireshark.'
    - R: 'Use Tcpdump.'

  - R: 'Setup Kali Linux virtual machine.'

LaterRequirements:
  - R: 'for 27001/2 SANS is required'
  - R: 'Use Metasploit.'
  - R: scapy
  - R: 'Analyze binary data'
  - R: 'Developer fuzzers.'
  - R: 'Explain why some consider CEH a "meme".'
  - R: 'Code in C.'
  - R: 'Code in Assembly x86-64.'
  - R: 'Read multiple programming languages.'
    - R: 'Read Ruby.'
    - R: 'Read PHP.'
    - R: 'Read Lua.'
    - R: 'Read Perl.'
    - R: 'Read Java.'
    - R: 'Read C#.'
    - R: 'Read Python.'
  -R: 'Explain and demonstrate use of gcc.'
  -R: 'Use extended virtual server software'
    -R: 'Use Hyper-V'
    -R: 'Use Xen'
    -R: 'Use KVM'
  - R: 'Write or modify Metasploit modules.'

---

# Offensive Security Certified Professional (Study)

## Language Learning Suggested Order for Pentesting

1. HTML
1. CSS
1. JavaScript (DOM only, no frameworks!)
1. Go
1. Bash
1. Python3 

1. PHP (for pwning)
1. C
1. Assembly (x86/x86-64/ARM)

Then *all* of the other languages for *reading* but especially:

1. Ruby
1. Perl
1. PowerShell

## Stuff *Not* Required but Good for Later (or Before)

* HTML (RW)
* CSS (RW)
* JavaScript (RW)
* C (RW)
* Go (RW)
    * <https://github.com/tomnomnom>
    * <https://github.com/michenriksen/aquatone>
* Assembly x86-64 (RW)
* Assembly x86 (RW)
* Lua (NMAP Scripting Engine, NSE)

## Tools

* `nmap` - enumeration
* `masscan` - enumeration
* `impacket` - modern `smbclient` alternative
* `evil-winrm` - <https://github.com/Hackplayers/evil-winrm>

For later:

* `zmap` - scan the Internet

## Languages Required to *Write*

* Bash (or POSIX?)
* Python2/3 and/or Perl

## Languages Required to *Read*

* PHP 
* JavaScript?

## Minimum Viable PC

* Intel i7+ or Ryzen 7 (4+ cores)
* Nvidia Card (CUDA)

## General Tips from OSCP Recipients

* Get good at taking notes (MD) and screenshots.
* Don't over focus on assembly.
* Take breaks and come back to hard problems.
* Know super-duper basic buffer overflows.
* IPPSEC HTB videos are best for OSCP prep
* OSCP is really more about avoiding rabbit holes.
* Go is becoming super popular in infosec space.
* Its fast, easy written, and the compiling binaries is huge.
* MSF is not needed at all, but you can use on *one* system.

## Language Required to *Read*

## Must Read Books

* [The Art of Exploitation](https://archive.org/details/hackingtheartofexploitation_202003/mode/2up) [ISO](https://gist.github.com/lzutao/ed4c88364d7732d14e96698ac0ad5176)

## Important Web Sites

* <https://www.offensive-security.com/pwk-oscp/>

* [OSCP Prep Guide from Community Manager for Offensive Security](https://www.netsecfocus.com/oscp/2019/03/29/The_Journey_to_Try_Harder-_TJNulls_Preparation_Guide_for_PWK_OSCP.html)

* <https://vulnhub.com>
* <https://cve.mitre.org> (also exploit-db)
* <http://shell-storm.org>
* <https://github.com/justinsteven/dostackbufferoverflowgood>
* <https://ippsec.rocks>
* <https://www.offensive-security.com/metasploit-unleashed/>
* <https://osintframework.com/>
* <https://github.com/sleventyeleven/linuxprivchecker/blob/master/linuxprivchecker.py>
* <https://www.regular-expressions.info/>
* <https://liveoverflow.com>
* <http://exploit-exercises.lains.space/protostar/>
* <https://github.com/offensive-security/exploitdb-papers>
* <https://github.com/zardus/wargame-nexus>
* <http://0xc0ffee.io/blog/OSCP-Goldmine>
* <https://workflowy.com/s/HMXa.18Kns7Dw2L>
* <https://book.hacktricks.xyz/pentesting/pentesting-network>
* <https://ctftime.org/writeup/18640>
* <https://www.vortex.id.au/2017/05/oscp-exam-preparation-exam-day-report-day/>

### Degree Granting Institutions

* Carnegie Mellon
* Dakota State University
* Western Governors University

### Hacker Games

#### Beginner

* <https://picoctf.com>
* <https://tryhackme.com>

#### Advanced

* <https://hackthebox.eu>

## Terminology

### Capture the Flag (CTF)

### Metasploit (MSF)

Swiss army knife for compromising systems (by script kiddies).

### Metaspoitable

Deliberately compromised system with multiple vulnerabilities for practicing use of Metasploit specifically.

### Shellcode

Code that creates a shell on a compromised system usually written in Assembly or C so that is fits into the smallest possible memory space. This is *not* shell code as in shell scripting. 

### WarGames

## Potential Employers

## Industry Culture

### Divide Between Government, Enterprise, and Freelance

The more conservative the employer, the more they tend to trust certificates and credentials.

### "CEH is a meme"

![CEH Meme](CEH.jpg)

![CEH Wonka Meme](ceh-wonka.jpg)

* The [DOD Requires](https://www.leaderquestonline.com/blog/is-getting-your-certified-ethical-hacker-ceh-worth-it/)
* "Surely it can't hurt"
* "OSCP & OSCE won’t hurt your chances, CEH might." ([F-Security](OSCP & OSCE won’t hurt your chances, CEH might.))
