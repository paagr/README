# Eloquent JavaScript (Annotations)

## Chapter 2

In the following `Number()` is *not* a *cast* (if you know what that is). It doesn't force the string into being a number. Instead, it returns `NaN` if its argument is not a number. This then has to be checked to be sure that the string entered in response to the `prompt()` is actual a number and not something else.
