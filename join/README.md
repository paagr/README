---
Title: Join the Community
---

Currently the community only meets remotely online, primarily live through Twitch (and Twitch IRC), but also on Discord. YouTube is simply a place to store past videos (which are delayed until 24 hours after creation due to Twitch terms of service).

---------- -------------------------------------------------
 Twitch    <https://rwxrob.live>  
 Discord   <https://https://discordapp.com/invite/7s35TRB>  
 YouTube   <https://www.youtube.com/c/rwxrob>
---------- ------------------------------------------------

