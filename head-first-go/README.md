---
Title: Head First Go
---

Even though *Head First Go* is *very* out of date it is currently the best Go programming language book for beginners. It is available for [purchase](https://www.amazon.com/Head-First-Go-Jay-McGavren/dp/1491969555) and online as PDF for free (although it is difficult to find and illegal in most countries).

## Before You Begin

Before you get into the book here's some stuff you'll need to do and consider before continuing.

### Install the Latest Go

First check to see that you don't already the right version of Go installed:

```sh
go --version
```

The current version (as of April 23, 2020) is 1.14.2.

You can certainly following the [installation instructions](https://golang.org/doc/install) on the Go page. Or you can add this little script to manage your Go version installation like any other package on your system. Keep in mind that although this PPA is non-standard and not supported by Go officially it is used by thousands.

```sh
#!/bin/sh

sudo add-apt-repository ppa:longsleep/golang-backports
sudo apt update
sudo apt install golang-go
```

You'll need to do your own research for other non-Debian versions of Linux (but you are probably used to that if you do).

:::callout-fyi
If you absolutely have no other option you can always just create a Golang project on [REPL.it](https://repl.it).
:::

### Getting Vim Ready for Go Coding

The [`fatih/vim-go`](https://github.com/fatih/vim-go) plugin is pretty standard albeit annoying at times. Don't forget to Install all the `vim-go` dependencies with `:GoInstallBinaries` from within a Vim session if you choose to use the following:

```vim
" Install the Plug plugin manager if not detected.
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall
endif

" If Plug plugin manager detected then load the plugins and configure
if filereadable(expand("~/.vim/autoload/plug.vim"))
  call plug#begin('~/.vimplugins')
  " ... other plugins here
  Plug 'fatih/vim-go'
  call plug#end()
  let g:go_fmt_fail_silently = 0     " let me out even with errors
  let g:go_fmt_command = 'goimports' " autoupdate import
  let g:go_fmt_autosave = 1          " autosave on updates

" Otherwise fallback to some safe backups
else
  autocmd vimleavepre *.go !gofmt -w % " backup if fatih fails
endif

```

## Chapter One: Let's Get Going: Syntax Basics

Hello world is hello world. Nothing fancy to see there.

Use the Go community convention of naming the file with the `func main()` in it `main.go` so you can find it easier.

Here's a version that introduces emojis as *runes* as well as the `fmt.Printf()` function:

```go
package main

import (
	"fmt"
	"math"
	"strings"
)

func main() {
	fmt.Println(math.Floor(2.75))
	fmt.Println(strings.Title("head first go"))
	a := '🙂'
	fmt.Printf("The letter '%c': %b\n", a, a)
}
```

<https://rwx.gg/head-first-go/chapter1/main.go>


TODO

## Frequently Asked Questions (FAQ)

Here are the most frequently asked questions about these annotations and the *Head First Go* book itself.

### Where can I get the PDF of the book?

Online. But using a PDF without having paid for the book is illegal in most countries (and some would argue even *with* having purchased the book). Please do not ask anyone from the rwx.gg team to provide you with a PDF. If you want one you'll need to find one on your own.

### Can I use Goland?

Sure. Goland is an incredible graphic development environment for Go that can be particularly useful for very large Go projects. In general graphic IDEs can make development tasks very efficient (such as renaming functions throughout the entire code base). However, these annotations assume you are using Vim have mastered Bash enough to do these same important tasks from the command line instead.

### Can I use Visual Studio Code (VSCode)?

Using anything but `vim` from the `bash` command line is strongly discouraged for reasons covered in the [BLiP Boost](/blip-boost/). But you can certainly use VSCode (or any editor for that matter) if you really want to. No time is spent explaining how so please *do not ask how to configure it during sessions*. 

## Chapter 8 {#chapter-8}

TODO
