---
Title: Copyright and Licensing
---

Except where otherwise noted, content is copyright 2020 Rob Muhlestein and licensed to all under a [*Creative Commons Attribution 4.0 International*](https://creativecommons.org/licenses/by-sa/4.0/) license. The code is released without warranty into the public domain. If you need to create a PDF of any specific page usually the printed copyright at the bottom of each page fulfill these requirements if unaltered.

![Creative Commons Attribution ShareAlike + Public Domain](cc-by-sa.png){.invertable}

## Content

You are free to do the following:

* copy and redistribute the material in any medium or format;

* remix, transform, and build upon the material for any purpose, even commercially.

You may do so only on the following terms:

* You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.

* If you remix, transform, or build upon the material, you must distribute your contributions under the same license as the original.

* You may not apply legal terms or technological measures that legally restrict others from doing anything this license permits.

## *All Rights Reserved*

Did you know you have not needed to put *all rights reserved* on anything for several years now?

The words have “no legal significance” [according to Ius mentis](https://www.iusmentis.com/copyright/allrightsreserved/) and originated because of an obscure treaty called the *Buenos Aires Copyright Convention* which has been superseded by the *Berne Convention*. 

So to all those out there who still think you need them, you don't.

## Trademarks and Reserved Copyright

The following --- and their equal with inverted colors --- are trademarks of Rob Muhlestein:

* [RWX.GG]{.spy}
* `rwx.gg`
* `rwxrob.live`
* Linux Beginner Boost
