---
Title: RWX, A Mantra for Life-Long Learning
Subtitle: Read/Research. Write. Execute/Exercise/Experience.
---

This transfer of knowledge has a rather simple formula. (Usually we start with *reading* but the cycle actually begins with *executing*):

* [**Executing, Exercising, and Experiencing.**](/x/) Getting experience to doing things, trying things, failing and succeeding.

* [**Writing.**](/w/) Capturing and sharing our experience in words and images through writing.

* [**Reading and Research.**](/r/) Reading the writing of others and experiencing for ourselves.

This cycle repeats both individually and collectively. Usually we enter the cycle when we learn of others experience through reading.

