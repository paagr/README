---
Title: Getting Started
---

## Day 1

1. Welcome and Introductions
1. [Course Overview and Goals](/goals/)
1. [Open Credentials](/open-credential/)
1. [Course Rules](/rules/)
1. [Learning to Learn (Summary, More Later)](/learning/)
1. Better Ways to Use [Keyboard](/typing/) and [Mouse](/mouse/)

## Day 2

1. [Set Up Brave](/brave/)
1. [Set Up Protonmail](/protonmail/)
1. Set Up [Discord](/discord/) and [IRC](/irc/)
1. Set Up [GitLab](/gitlab/) and [GitHub](/github/)

## Day 3

1. Learn GitLab Editor
1. Create a Codebook
1. Publish Codebook to Netlify
1. Publish First JAMStack Web Page
1. Other Collaborative Coding Sites
    1. REPL.it
    1. Glitch.com

## Day 4

1. Discussion on Personal Safety
1. Set Up KeePassXC

## Day 5
