---
Title: Vi/m Text Editor
---

Vi was created in 1976 and is the second^*^ oldest editor on the planet --- and the most ubiquitous. It's everywhere. For that reason anyone wanting a career involving Linux and UNIX systems should learn it even if they prefer something else. Vim (Vi-improved) is a larger editor with many great additions, some of which you should avoid so as to not lock-in bad muscle-memory when you are on a system that only has `vi` installed (the arrow keys for example).

^*^ `ed` is the first, and still damn valuable to learn as well.

## Discussion Questions

* So how do I know if it is `vi` or `vim`?

Install `nvi` and try it in that. `nvi` is a tool created to *exactly* model everything --- including the bugs --- of the original `vi` editor.

## Resources

* <http://yannesposito.com/Scratch/en/blog/Learn-Vim-Progressively/>
