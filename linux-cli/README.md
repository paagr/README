---
Title: The Linux Command Line (Annotations)
---

The freely distributable book [The Linux Command Line](the-linux-command-line-2nd-ed.pdf) is currently the most comprehensive and up-to-date introduction to the Linux Bash command line. It is the only book in print that covers Bash 4+ associative arrays, for example. William Shotts is a great writer with an easy to read style. The following annotations are designed to clarify and update the reader when needed (as well as to point out the rare errors that occur in the book).

## Introduction

Be sure you read all of this.

## Chapter 1

TODO

## Chapter 24: Writing Your First Script {#chapter-24}

TODO
