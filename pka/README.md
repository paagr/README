---
Title: Progressive Knowledge Apps
---

A *progressive knowledge app* is a [progressive web app](/pwa/) that is not just for the web being built on the foundation of [Pandoc Markdown](/markdown/) allowing for rending in any number of consumable media formats including ePub, Kindle, PDF, and others.

