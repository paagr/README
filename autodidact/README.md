---
Title: Becoming an Autodidact
---

An *autodidact* is one who takes learning into their own hands either because they prefer it or are forced to for lack of other resources. An autodidact reads, writes meticulous notes, and executes tests to gain new knowledge and skills. Failure is an integral part of the learning process to an autodidact. They don't fear it, they seek it and are intrigued by it.

## Autodidact 

Becoming an [autodidact](/autodidact/) is the most essential skill anyone will ever master. Without it you simply cannot progress on your own. So many enter the workplace without learning this important skill. Perhaps you have encountered them. They tend to come over and ask a lot of questions and usually come off as needy and unprofessional. Don't be that person. Learn to do your own research, answer your own questions, and really *look* at things.

Ironically few traditional schools spend any time at all helping you learn this. It is almost as if such organizations were motivated to make you dependent on *them* instead of yourself, but probably not.

Those who study humanity say that the collective ability to pass on knowledge to the next generation even after we have died is the single greatest reason humans have rocketed past all other species in growth and progress. Whether you believe this is because of natural science or some greater metaphysical force it is a fact we can observe.




## Assess and Evaluate

* Thinking Critically (Know When to Avoid StackExchange)

## Books and Resources

* [Deep Work](https://www.amazon.com/Deep-Work-Focused-Success-Distracted/dp/1455586692)
