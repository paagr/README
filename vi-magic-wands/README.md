# Vi (Vim) Magic Wands FTW!

*Magic wands* are my mnemonic device for using the exclamation point (bang) to send the current line, section, or page to any command that can be run from the shell and replace those lines with the output of that command. In fact, once you master the magic wands you can use the same keystrokes to do find and replace and other tasks that use `ex` mode just by backspacing out the exclamation point.

::: callout-comment
Yeah, you don't need all those macros. I'm telling you.
:::

## Line Wand

The line wand is by far the most frequent wand you will use. It is the fastest and most common way to extend `vi`. Just spam bang twice to send the current line to the shell command or just replace the current line with the output of a utility command like `date`, `cal` or *any* text that comes out of *any* command.

### Send Line to Bash: `!!bash`

```bash
for i in {1..20}; do echo Item $i; done
```

### Send Line to Calculator: `!!bc`

```bc
232432 * 2342342 / 234
```

### Send Line to Python3: `!!python3`

```python
[print(f"Item {x}") for x in range (1,21)]
```

## Section Wand

Sending a section is a great way to run part of a larger script (say for configuration) without having to cut it out and put it into another file. If you use this easy way, don't forget to undo it later to replace it with the original code (instead of the output of the lines of code).

### Send Section to Bash: `!}bash`

```bash
for i in {1..20}
do
  echo Item $i
done
```

## Line Number Wand: `!:<lineafter>`

Line number wand is just a larger version of the [section wand] but allows blank lines to be included. It takes more keystrokes, however. Start with a bang and follow it up with a colon and the line *after* the last line that you want to include.

For the following example imagine the first line is line 10 of the file and you want to send it to `pandoc` for rending as HTML. You would position your cursor on the first `#` (on line 10) and type `!:22<enter>` (because there are 11 lines to send). Then type `pandoc<enter>` after the `!` as the command to receive the lines and replace them.

```markdown
# This is a title with *emphasis*

* one
* two
* three

How about a paragraph

## Thursday, March 12, 2020, 7:46:39PM

something
```

