---
Title: Course Session Schedule
---

The course is primarily organized by weeks. Each week is divided into five sessions matching the days of the week, Monday through Friday. The daily breakdown is a more of a guide and will vary significantly within any given week.

:::callout-fyi
Remember that you can work on things well in advance and reach out to your mentor or community for help as you need it. This weekly schedule simply helps guide the in-session discussion to keep it on topic.
:::

Each 3-hour session is divided into 40 minute flipped discussion segments dedicated to topical questions and answers and interspersed with 15-minute breaks. Each segment ends five minutes before the hour and starts ten minutes after the next:

-------- ------------------------
 11:00    Welcome
 11:10    **Segment One**
 11:55    Break
 12:10    **Segment Two**
 12:55    Break.
 01:10    **Segment Three**
 02:00    End
-------- ------------------------

Table: Session Time Breakdown

1. [Getting Started](/start/)
1. [Knowledge as Source](/knowledge/)
1. [Learning to Work and Learn](/learning/)
1. [Running The Linux Operating System](/linux/)
1. [The Linux Command Line, I-III (Book)](/linux-cli/)
1. [The Linux Command Line, IV Bash Scripts (Book)](/linux-cli/#chapter-24)
1. [Becoming a Terminal Master (Vim/TMUX/Lynx/SSH)](/terminal/)
1. [Networking and the Internet](/networking/)
1. [Learning Web Design, I,V HTML/Images (Book)](/learning-web-design/)
1. [Learning Web Design, II CSS (Book)](/learning-web-design/)
1. [Eloquent JavaScript, Chapters 1-11 (Book)](/eloquent-javascript/)
1. [Eloquent JavaScript, Chapters 12-21 (Book)](/eloquent-javascript/)
1. [Head First C, Chapters 1-6 (Book)](/head-first-c/)
1. [Head First C, Chapters 7-12 (Book)](/head-first-c/#chapter-7)
1. [Head First Go, Chapters 1-7 (Book)](/head-first-go/)
1. [Head First Go, Chapters 8-16 (Book)](/head-first-go/#chapter-8)
